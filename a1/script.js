//GET all 'title' in /todos
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	let list = data.map((todo)=>{
		return todo.title;
	})
	console.log(list)
})



// Getting specific todo list item using data.title and data.completed
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => console.log(`The item "${data.title}"has status of ${data.completed}`)); //false


//Creating a to do list using POST method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: "POST",
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));


//Updating a todo list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));

//Updating a todo list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PATCH",
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
	  	dateCompleted: '01/19/22'
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));


//Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE',
	headers:{
		'Content-Type': 'application/json'
	},
})
.then((response)=> response.json())
.then((data)=> console.log(data))

